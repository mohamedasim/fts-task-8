import { Component, Input, OnInit, Output } from '@angular/core';
import { StudentService } from 'src/app/services/student.service';
import { Student } from 'src/app/models/student.model';
import { Form } from '@angular/forms';
import { formatCurrency } from '@angular/common';
// import { MatSort } from '@angular/material';
@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  D: Student[];
  data: Array<any>;
  totalRecords: number;
  page: number = 1;
  showpopup=false;
  createstudent=false;
  studentid:any;
  editstudent=false;
  constructor(private studentService: StudentService)
  {this.data = new Array<any>()}

  ngOnInit(): void {
    this.getTableData();
  }

  onDelete(id: number) {

    this.studentService.onDelete(id);
    this.getTableData();
    
  }
  getTableData() {
    this.D = this.studentService.onGet();
  }
  showEditForm:boolean = false;
@Output() id:number=0;
  EditValue:number = 0;

  closeEditfromStudentPage() {
    debugger;
    this.showEditForm = false;
    this.getTableData();
  }

  openCreate() {
    debugger;
    this.showpopup=true;
    this.studentid="0";
    this.createstudent=true;
  }
  editPopup(event){
    this.showpopup=true;
    this.studentid=event;
    this.editstudent=true;
  }
  closepopup(){debugger
    this.showpopup=false;
    this.createstudent=false;
    this.editstudent=false;
    this.getTableData();
  }
}
