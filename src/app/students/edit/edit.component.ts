import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm, Validators, FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { StudentService } from 'src/app/services/student.service';
import { Student } from 'src/app/models/student.model';
import { empty } from 'rxjs';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  form = new FormGroup({
    id: new FormControl('',Validators.required),
    sname: new FormControl('',Validators.required),
    fname: new FormControl('',Validators.required),
    date: new FormControl('',Validators.required),
    dep: new FormControl('',Validators.required),
    address: new FormControl('',Validators.required),
    email: new FormControl('',[Validators.required, Validators.email]),
    age: new FormControl('',Validators.required),
    pass: new FormControl('',[Validators.required, Validators.minLength(6)]),
    repass: new FormControl('',[Validators.required, Validators.minLength(6)]),
    gender: new FormControl(''),
    checkbox: new FormControl('')
  });
  //id: number;
  header: string;
  student: Student = {
    id: 0,
    sname: '',
    fname: '',
    gender: '',
    date: '',
    dep: '',
    address: '',
    email: '',
    age: 0,
    pass: '',
    repass: '',
    checkbox: false,
  };
  isEdit=false;
  title="";
  constructor(private router: Router, private route: ActivatedRoute, private studentService: StudentService,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    debugger;
    this.getformValue();
  }

  @Input() showpopup:boolean =true;
  @Input() createstudent:boolean=false;
  @Input() editstudent:boolean=false;
  @Input() viewType:any = null;
  @Input() studentId:any;

  getformValue() {

    debugger;
    if (this.editstudent) {
      let student:Student = this.studentService.onGetStudent(this.studentId)[0];
     debugger;
      this.isEdit=true;
     this.form.patchValue({
      id:student.id,
    sname:student.sname,
    fname:student.fname,
    date: student.date,
    gender:student.gender,
    dep: student.dep,
    address:student.address,
    email:student.email,
    age: student.age,
    pass:student.pass,
    repass:student.repass,
    checkbox:student.checkbox
    })

    }

  }
  checkPasswords() { // here we have the 'passwords' group
  const password = this.form.value['pass'];
  const confirmPassword = this.form.value['repass'];

  return password === confirmPassword ? null : { notSame: true }     
}

  onSubmit() {

    debugger;
    
    alert(this.createstudent)
    let studentValue = this.form.value;

     if (!this.isEdit) {
      let rdata = this.studentService.onAdd(studentValue);
        alert("Created sucessfully");        
          this.router.navigate(['/']);
          this.form.reset()
    }
    else {
      this.studentService.onUpdate(studentValue);
      alert("updated sucessfully");
        this.router.navigate(['/']);
        this.isEdit=false;
        this.form.reset()
    }
  }
}

